import numpy as np
import os
from PIL import Image
import pydicom

def numpy_to_jpeg(
    rutaInput,
    rutaOutput,
    num_imagenes_a_extraer=None,
    porcentaje_de_recorte=None,
):
    mri = np.load(rutaInput)
    os.makedirs(rutaOutput, exist_ok=True)  # Crear carpeta si no existe
    cant_slide = mri.shape[0]

    if num_imagenes_a_extraer is not None:
        # Asegura que num_imagenes_a_extraer no es mayor a la cantidad de imágenes disponibles
        num_imagenes_a_extraer = min(num_imagenes_a_extraer, cant_slide)

        # Calcula el índice inicial para extraer las imágenes del centro
        inicio = (cant_slide - num_imagenes_a_extraer + 1) // 2

        for n in range(inicio, inicio + num_imagenes_a_extraer):
            jpeg_path = os.path.join(rutaOutput, f"{n}.jpeg")
            imagen = Image.fromarray(mri[n])

            if porcentaje_de_recorte is not None:
                imagen = zoom_image(imagen, porcentaje_de_recorte)

            imagen.save(jpeg_path)
    else:
        for n in range(cant_slide):
            jpeg_path = os.path.join(rutaOutput, f"{n}.jpeg")
            Image.fromarray(mri[n]).save(jpeg_path)

def zoom_image(imagen, porcentaje_de_recorte):
    ancho, alto = imagen.size
    nuevo_ancho = int(ancho * (1 - porcentaje_de_recorte))
    nuevo_alto = int(alto * (1 - porcentaje_de_recorte))

    # Calcula las coordenadas de recorte
    izquierda = (ancho - nuevo_ancho) // 2
    arriba = (alto - nuevo_alto) // 2
    derecha = ancho - izquierda
    abajo = alto - arriba

    # Aplica el recorte y devuelve la imagen recortada
    imagen_recortada = imagen.crop((izquierda, arriba, derecha, abajo))
    return imagen_recortada

def dicom_to_jpeg(dicom_directory, output_directory):
    # Crear un diccionario para organizar las imágenes por serie
    series_dict = {}

    # Escanea el directorio DICOM y organiza las imágenes
    for root, dirs, files in os.walk(dicom_directory):
        for file in files:
            dicom_file = os.path.join(root, file)
            ds = pydicom.dcmread(dicom_file)

            # Obtiene la información de la serie o secuencia
            series_description = ds.ProtocolName
            image_number = ds.InstanceNumber

            # Crea una carpeta para la serie si no existe
            series_directory = os.path.join(output_directory, series_description)
            if not os.path.exists(series_directory):
                os.makedirs(series_directory)

            # Convertir la imagen DICOM a JPEG
            img = Image.fromarray(
                (ds.pixel_array / ds.pixel_array.max() * 255).astype("uint8")
            )

            # Guardar la imagen como JPEG
            img.save(os.path.join(series_directory, f"image_{image_number}.jpeg"))

            # Actualizar el diccionario de series
            if series_description in series_dict:
                series_dict[series_description].append(image_number)
            else:
                series_dict[series_description] = [image_number]

def edit_jpeg(
    rutaInput,
    rutaOutput,
    num_imagenes_a_extraer=None,
    porcentaje_de_recorte=None,
):

    archivos = os.listdir(rutaInput)
    archivos = sorted(os.listdir(rutaInput), key=lambda x: int(x.split(".")[0]))
    total_archivos = len(archivos)
    for archivo in archivos:
       
        # Calcula el índice inicial para extraer las imágenes del centro
        inicio = (total_archivos - num_imagenes_a_extraer + 1) // 2

        for i in range(inicio, inicio + num_imagenes_a_extraer):
            archivo_original = os.path.join(rutaInput, archivos[i])
            archivo_destino = os.path.join(rutaOutput, archivos[i])

            # Cargar la imagen original
            imagen = Image.open(archivo_original)

            # Aplicar el zoom a la imagen (por ejemplo, un factor de 2 para duplicar el tamaño)
            imagen_zoom = zoom_image(imagen, porcentaje_de_recorte)

            # Guardar la imagen con zoom en la nueva ubicación
            imagen_zoom.save(archivo_destino, "JPEG")
